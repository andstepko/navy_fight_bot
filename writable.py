class Writable:

    def write(self, text_to_write):
        raise NotImplementedError("Class %s doesn't implement write()." % self.__class__.__name__)
