#!/usr/bin/python
# -*- coding: UTF-8 -*-
import json
import urllib
from urllib.parse import urlencode
import urllib.request
import codecs
import time
import config_manager
import os

MESSAGES = "messages"
WALL = 'wall'

SEND_METHOD_NAME = "send"
GET_METHOD_NAME = "get"
MARK_READ_METHOD_NAME = 'markAsRead'

USER_ID = "user_id"
CHAT_ID = 'chat_id'
DOMAIN = 'domain'
OWNER_ID = 'owner_id'
OFFSET = 'offset'
MESSAGE = "message"
ATTACHMENT = 'attachment'
MESSAGE_IDS = 'message_ids'
COUNT = "count"
FILTERS = "filters"
VERSION = "version"


def call_api(method, params):
    params.append(("access_token", get_token()))

    captcha_sid, captcha_key = obtain_captcha_sid_and_key()
    if captcha_sid is not None:
        print('call_api().using captcha.')
        params.append(("captcha_sid", captcha_sid))
        params.append(("captcha_key", captcha_key))
        remove_captcha_file()

    url = "https://api.vk.com/method/%s?%s" % (method, urlencode(params))

    reader = codecs.getreader("utf-8")
    obj = json.load(reader(urllib.request.urlopen(url)))
    return obj


def get_token():
    file_path = config_manager.get_value('TOKEN_FILE_NAME')
    try:
        file = open(file_path)
    except FileNotFoundError:
        return None

    token = file.readline()
    if token[len(token) - 1] == '\n':
        token = token[:len(token) - 1]
    file.close()
    return token


def obtain_captcha_sid_and_key():
    file_path = config_manager.get_value('CAPTCHA_FILE_NAME')
    try:
        file = open(file_path)
    except FileNotFoundError:
        return None, None

    captcha_sid = file.readline()
    captcha_key = file.readline()
    if captcha_sid[len(captcha_sid) - 1] == '\n':
        captcha_sid = captcha_sid[:len(captcha_sid) - 1]
    if captcha_key[len(captcha_key) - 1] == '\n':
        captcha_key = captcha_key[:len(captcha_key) - 1]

    file.close()
    return captcha_sid, captcha_key


def remove_captcha_file():
    captcha_file_path = config_manager.get_value('CAPTCHA_FILE_NAME')
    try:
        os.remove(captcha_file_path)
        return True
    except FileNotFoundError:
        return False


def send_message(user_id, text):
    obj = call_api(MESSAGES + "." + SEND_METHOD_NAME, [(USER_ID, user_id), (MESSAGE, text)])
    print('send_message().response==>' + str(obj))
    return obj


# TODO refactor me
def send_attachment(user_id, owner_id, media_id):
    attachment = 'wall' + str(owner_id) + '_' + str(media_id)
    obj = call_api(MESSAGES + "." + SEND_METHOD_NAME, [(USER_ID, user_id), (ATTACHMENT, attachment)])
    print('send_attachment().response==>' + str(obj))

    try:
        response_error_code = obj['error']['error_code']
        if response_error_code == 14:
            # Too often queries. (captcha needed)
            print('Too often queries.')
            time.sleep(5)
            obj = call_api(MESSAGES + "." + SEND_METHOD_NAME, [(USER_ID, user_id),
                                                               (ATTACHMENT, attachment)])
        else:
            print("send_attachment().Some strange error_code got from VK==>" + str(response_error_code))
    except KeyError:
        # No 'error_code'.
        return obj


def send_chat_message(chat_id, text):
    return call_api(MESSAGES + '.' + SEND_METHOD_NAME, [(CHAT_ID, chat_id), (MESSAGE, text)])


def get_unread_messages(count):
    return call_api(MESSAGES + "." + GET_METHOD_NAME, [(COUNT, count), (FILTERS, 1), (VERSION, 5.4)])


def get_group_posts(domain, count):
    return call_api(WALL + '.' + GET_METHOD_NAME, [(DOMAIN, domain), (COUNT, count)])


def mark_read(mid):
    return call_api(MESSAGES + '.' + MARK_READ_METHOD_NAME, [(MESSAGE_IDS, mid)])
