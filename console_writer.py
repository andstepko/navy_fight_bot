from writable import Writable


class ConsoleWriter(Writable):

    def write(self, text_to_write):
        print(text_to_write)
