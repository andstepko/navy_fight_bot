# -*- coding: utf-8 -*-
from navy_fight.user_games_manager import UserGamesManager
from vk_writer import VKWriter
from console_writer import ConsoleWriter


def main(debug):
    user_id = '40435728'
    if debug:
        user_games_manger = UserGamesManager(user_id, ConsoleWriter())
    else:
        user_games_manger = UserGamesManager(user_id, VKWriter(user_id))

    # sys.stdin = codecs.getreader('utf-8')(sys.stdin)
    while 1:
        user_input = input('\n==>')
        user_games_manger.process_user_request(user_input)


main(False)
