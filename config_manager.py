import os
import configparser


FOLDER_PATH = os.path.dirname(os.path.realpath(__file__))
CONFIG_FILE_PATH = FOLDER_PATH + '/config.ini'


def get_value(key):
    config = configparser.ConfigParser()
    config.read(CONFIG_FILE_PATH)
    return config.get('DEFAULT', key)
