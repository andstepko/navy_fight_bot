from random import randint

from navy_fight.ship import Ship

EMPTY = 0
SHIP = 1
BUFFER_ZONE = 2
SHOT = 4
INJURED = 8
DEAD = 16

FIELD_SIZE = 10
CLASSIC_SHIPS = [(4, 1), (3, 2), (2, 3), (1, 4)]


class Game:

    def __init__(self, private_field=None, public_field=None):
        if private_field is None or public_field is None:
            self.__clear_fields()
            self.__put_ships(CLASSIC_SHIPS)
        else:
            self.__field = private_field
            self.__user_field = public_field

    def shoot(self, point):
        i = point[0]
        j = point[1]

        if i >= FIELD_SIZE or j >= FIELD_SIZE:
            return self.__user_field

        self.__user_field[i][j] |= SHOT
        if self.__field[i][j] & SHIP == SHIP:
            self.__user_field[i][j] |= INJURED

            ship = self.__get_whole_ship(point)
            if self.__is_whole_ship_injured(ship):
                self.__mark_ship_dead(ship)
                Game.__surround_with_buffer_zone(ship, self.__user_field)

        return self.__user_field

    def get_user_field(self):
        return self.__user_field

    def get_private_field(self):
        return self.__field

    def __clear_fields(self):
        self.__field = []
        self.__user_field = []

        for i in range(FIELD_SIZE):
            self.__field.append([0] * FIELD_SIZE)
            self.__user_field.append([0] * FIELD_SIZE)

    def __get_whole_ship(self, ship_point):
        i = ship_point[0]
        j = ship_point[1]

        if self.__field[i][j] & SHIP == 0:
            return None

        if (j > 0 and self.__field[i][j - 1] & SHIP == SHIP) or\
                (j < (len(self.__field[0]) - 1) and (self.__field[i][j + 1] & SHIP == SHIP)):
            # Horizontal ship
            return self.__get_whole_horizontal_ship(ship_point)
        elif (i > 0 and self.__field[i - 1][j] & SHIP == SHIP) or \
                (i < (len(self.__field) - 1) and (self.__field[i + 1][j] & SHIP == SHIP)):
            # Vertical ship
            return self.__get_whole_vertical_ship(ship_point)
        else:
            return Ship([ship_point])

    def __get_whole_horizontal_ship(self, ship_point):
        i = ship_point[0]
        j = ship_point[1]

        cur_j = j
        while cur_j >= 0 and self.__field[i][cur_j] & SHIP == SHIP:
            cur_j -= 1
        left_edge = cur_j + 1

        cur_j = j
        while cur_j <= (len(self.__field[0]) - 1) and self.__field[i][cur_j] & SHIP == SHIP:
            cur_j += 1
        right_edge = cur_j - 1

        return Ship.build_horizontal((i, left_edge), right_edge - left_edge + 1)

    def __get_whole_vertical_ship(self, ship_point):
        i = ship_point[0]
        j = ship_point[1]

        cur_i = i
        while cur_i >= 0 and self.__field[cur_i][j] & SHIP == SHIP:
            cur_i -= 1
        top_edge = cur_i + 1

        cur_i = i
        while cur_i <= (len(self.__field) - 1) and self.__field[cur_i][j] & SHIP == SHIP:
            cur_i += 1
        bottom_edge = cur_i - 1

        return Ship.build_vertical((top_edge, j), bottom_edge - top_edge + 1)

    def __is_whole_ship_injured(self, ship):
        for i, j in ship.get_points():
            if self.__user_field[i][j] & INJURED != INJURED:
                return False
        return True

    def __mark_ship_dead(self, ship):
        for i, j in ship.get_points():
            self.__user_field[i][j] |= DEAD

    # Put ships
    def __substitute_cells(self, old_cell, new_cell):
        for row in self.__field:
            for i in range(len(row)):
                if row[i] == old_cell:
                    row[i] = new_cell

    def __put_ships(self, ships_parameters):
        for deck_length, ships_number in ships_parameters:
            for i in range(ships_number):
                if not self.__put_ship(deck_length):
                    raise RuntimeError("ERROR: Unable to put a ship: deck_length==>"
                                       + str(deck_length))

    def __put_ship(self, deck_length):
        ship_alternatives = self.__prepare_put_alternatives(deck_length)
        if len(ship_alternatives) == 0:
            return False
        else:
            chosen_ship = Game.__choose_item_randomly(ship_alternatives)
            self.__put_ship_into_field(chosen_ship)
            return True

    def __prepare_put_alternatives(self, deck_length):
        alternatives = self.__prepare_horizontal_put_alternatives(deck_length)
        if deck_length > 1:
            alternatives += self.__prepare_vertical_put_alternatives(deck_length)
        return alternatives

    @staticmethod
    def __choose_item_randomly(alternatives):
        i = randint(0, len(alternatives) - 1)
        return alternatives[i]

    def __put_ship_into_field(self, ship):
        for i, j in ship.get_points():
            if self.__field[i][j] != EMPTY:
                raise RuntimeError("Cannot put the ship, cell is not EMPTY.ship==>" +
                                   str(ship) + " field==>\n" + str(ship))
            else:
                self.__field[i][j] |= SHIP
        Game.__surround_with_buffer_zone(ship, self.__field)

    @staticmethod
    def __surround_with_buffer_zone(ship, field):
        buffer_cells = Game.__get_buffer_zone(ship, field)
        for i, j in buffer_cells:
            if (0 <= i < len(field)) and (0 <= j < len(field[0])):
                field[i][j] |= BUFFER_ZONE

    @staticmethod
    # TODO Refactor me.
    def __get_buffer_zone(ship, field):
        buffer_cells = []
        if ship.is_horizontal():
            for i, j in ship.get_points():
                buffer_cells.append((i - 1, j))
                buffer_cells.append((i + 1, j))
            # To left of the ship
            first_ship_cell = ship.get_first_point()
            if first_ship_cell[1] > 0:
                buffer_cells.append((first_ship_cell[0] - 1, first_ship_cell[1] - 1))
                buffer_cells.append((first_ship_cell[0], first_ship_cell[1] - 1))
                buffer_cells.append((first_ship_cell[0] + 1, first_ship_cell[1] - 1))
            # To right of the ship
            last_ship_cell = ship.get_last_point()
            if last_ship_cell[1] < len(field[0]) - 1:
                buffer_cells.append((last_ship_cell[0] - 1, last_ship_cell[1] + 1))
                buffer_cells.append((last_ship_cell[0], last_ship_cell[1] + 1))
                buffer_cells.append((last_ship_cell[0] + 1, last_ship_cell[1] + 1))
        else:
            for i, j in ship.get_points():
                buffer_cells.append((i, j - 1))
                buffer_cells.append((i, j + 1))
            # To top of the ship
            first_ship_cell = ship.get_first_point()
            if first_ship_cell[0] > 0:
                buffer_cells.append((first_ship_cell[0] - 1, first_ship_cell[1] - 1))
                buffer_cells.append((first_ship_cell[0] - 1, first_ship_cell[1]))
                buffer_cells.append((first_ship_cell[0] - 1, first_ship_cell[1] + 1))
            # To bottom of the ship
            last_ship_cell = ship.get_last_point()
            if last_ship_cell[0] < len(field) - 1:
                buffer_cells.append((last_ship_cell[0] + 1, last_ship_cell[1] - 1))
                buffer_cells.append((last_ship_cell[0] + 1, last_ship_cell[1]))
                buffer_cells.append((last_ship_cell[0] + 1, last_ship_cell[1] + 1))
        return buffer_cells

    # region Horizontal alternatives
    def __prepare_horizontal_put_alternatives(self, deck_length):
        ship_alternatives = []
        row_index = 0
        for row in self.__field:
            ship_alternatives += Game.__prepare_row_put_alternatives(row, row_index, deck_length)
            row_index += 1
        return ship_alternatives

    @staticmethod
    def __prepare_row_put_alternatives(row, row_index, deck_length):
        ship_alternatives = []

        empty_spans = Game.__find_empty_spans(row)
        for empty_span in empty_spans:
            span_start = empty_span[0]
            span_length = empty_span[1]
            if span_length < deck_length:
                continue
            else:
                for i in range(span_length - deck_length + 1):
                    start_coordinate = (row_index, span_start + i)
                    ship_alternatives.append(Ship.build_horizontal(start_coordinate, deck_length))
        return ship_alternatives

    @staticmethod
    def __find_empty_spans(series):
        empty_spans = []
        empty_cells_counter = 0
        span_starting_index = 0
        cur_cell_index = 0
        for cell in series:
            if cell == EMPTY:
                empty_cells_counter += 1
            else:
                if empty_cells_counter > 0:
                    empty_spans.append((span_starting_index, empty_cells_counter))
                    empty_cells_counter = 0
                span_starting_index = cur_cell_index + 1
            cur_cell_index += 1
        if empty_cells_counter > 0:
            empty_spans.append((span_starting_index, empty_cells_counter))
        return empty_spans
    # endregion Horizontal alternatives

    # region Vertical alternatives
    def __prepare_vertical_put_alternatives(self, deck_length):
        alternatives = []
        row_index = 0
        for column_index in range(0, len(self.__field[0])):
            column = self.__get_column(column_index)
            alternatives += Game.__prepare_column_put_alternatives(
                column, column_index, deck_length)
            row_index += 1
        return alternatives

    def __get_column(self, column_index):
        column = []
        for i in range(len(self.__field)):
            column.append(self.__field[i][column_index])
        return column

    @staticmethod
    def __prepare_column_put_alternatives(column, column_index, deck_length):
        ship_alternatives = []

        empty_spans = Game.__find_empty_spans(column)
        for empty_span in empty_spans:
            span_start = empty_span[0]
            span_length = empty_span[1]
            if span_length < deck_length:
                continue
            else:
                for i in range(span_length - deck_length + 1):
                    ship_alternatives.append(Ship.build_vertical(
                    (span_start + i, column_index), deck_length))
        return ship_alternatives
    # endregion Vertical alternatives
