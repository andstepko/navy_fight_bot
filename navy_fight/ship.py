class Ship:

    points = []

    def __init__(self, points):
        self.points = points

    @classmethod
    def build_horizontal(cls, start_coordinate, deck_length):
        points = []
        for i in range(start_coordinate[1], start_coordinate[1] + deck_length):
            points.append((start_coordinate[0], i))
        return Ship(points)

    @classmethod
    def build_vertical(cls, start_coordinate, deck_length):
        points = []
        for i in range(start_coordinate[0], start_coordinate[0] + deck_length):
            points.append((i, start_coordinate[1]))
        return Ship(points)

    def get_points(self):
        return self.points

    def is_horizontal(self):
        if len(self.points) <= 1:
            return True
        else:
            first_ship_cell = self.points[0]
            second_ship_cell = self.points[1]
            return first_ship_cell[0] == second_ship_cell[0]

    def get_first_point(self):
        return self.points[0]

    def get_last_point(self):
        return self.points[len(self.points) - 1]


