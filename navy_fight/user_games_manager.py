from navy_fight.game import Game
from navy_fight.game import SHIP, EMPTY, BUFFER_ZONE, SHOT, INJURED, DEAD
import os
import ast

NEW_GAME = 'new'
HELP = 'help'
FIELD = 'field'
PRIVATE_FIELD = 'private field'
STOP = 'stop'

EMPTY_SMILE = '&#128034;'  # green
DEAD_SMILE = '&#11035;'  # black
INJURED_SMILE = '&#128520;'  # happy devil   '&#127384;' - SOS
SHOT_BUFFER_ZONE_SMILE = '&#128044;'  # dolphin
UNSHOT_BUFFER_ZONE_SMILE = '&#128031;'  # blue fish
SHOT_SMILE = '&#128037;'  # duck

SHIP_SMILE = '&#128674;'
ROCKET_SMILE = '&#128640;'

NUMBERS_SMILES = ['1&#8419;', '2&#8419;', '3&#8419;', '4&#8419;', '5&#8419;',
                  '6&#8419;', '7&#8419;', '8&#8419;', '9&#8419;', '&#128287;']
LETTERS_SMILES = ['&#127344;', '&#127345;', '&#127346;', '&#127347;', '&#127348;',
                  '&#127349;', '&#127350;', '&#127351;', '&#127352;', '&#127353;']

# ALPHABET = [u'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К']
ALPHABET = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']

OUTPUT_DEBUG = False

FOLDER_PATH = os.path.dirname(os.path.realpath(__file__)) + '/navy_fight_data/'


class UserGamesManager:

    def __init__(self, user_id, writer):
        self.__user_id = user_id
        self.__writer = writer
        self.__game = Game(self.__obtain_private_field_from_file(),
                           self.__obtain_public_field_from_file())

    def process_user_request(self, user_input):
        user_input = user_input.lower()

        if user_input == NEW_GAME:
            self.__game = Game()
            self.__save_game()
            response = "OK. Let's start :-)\n"
            response += UserGamesManager.__build_matrix_string(self.__game.get_user_field())
        elif user_input == HELP:
            response = UserGamesManager.__build_user_help()
        elif user_input == FIELD:
            response = UserGamesManager.__build_matrix_string(self.__game.get_user_field())
        elif user_input == PRIVATE_FIELD:
            response = UserGamesManager.\
                __build_readable_matrix_string_with_ships_only(self.__game.get_private_field())
        else:
            try:
                coordinate_text = user_input.replace(' ', '')
                shoot_point = UserGamesManager.parse_coordinates_from_user_input(coordinate_text)
                user_field = self.__game.shoot(shoot_point)
                self.__save_game()
                response = UserGamesManager.__build_matrix_string(user_field)
            except ValueError:
                response = "Command not found. Use '" + HELP + "' for help."
        self.__writer.write(response)

    @staticmethod
    def __build_user_help():
        result = EMPTY_SMILE + ' - empty cell.\n'
        result += DEAD_SMILE + ' - dead ship.\n'
        result += INJURED_SMILE + ' - injured ship.\n'
        result += SHOT_BUFFER_ZONE_SMILE + " - ship's buffer zone, which was shot.\n"
        result += UNSHOT_BUFFER_ZONE_SMILE + " - ship's buffer zone, which was NOT shot.\n"
        result += SHOT_SMILE + ' - shot empty cell.\n\n'

        result += "Use '" + NEW_GAME + "' to start new game.\n"
        result += "Use '" + FIELD + "' to see your gaming field.\n"
        result += "Use letter and number (e.g. C7) to make a shoot.\n"
        result += "Use '" + HELP + "' to see this message.\n"
        result += "Use '" + STOP + "' to pause game and exit play mode.\n"
        return result

    @staticmethod
    def parse_coordinates_from_user_input(user_input):
        letter = user_input[0].upper()
        number = user_input[1:]

        i = int(number) - 1
        j = ALPHABET.index(letter)

        return i, j

    @staticmethod
    def __build_readable_matrix_string_with_ships_only(field):
        result_str = ''
        for row in field:
            for cell in row:
                if cell == SHIP:
                    result_str += UserGamesManager.__debug_cell_to_str(SHIP)
                else:
                    result_str += UserGamesManager.__debug_cell_to_str(EMPTY)
            result_str += '\n'
        return result_str[:len(result_str) - 1]

    @staticmethod
    def __build_matrix_string(field):
        result_str = ''
        if not OUTPUT_DEBUG:
            result_str += ROCKET_SMILE + ' '
            for alphabet_letter in ALPHABET:
                result_str += '-.' + alphabet_letter + ' '
            # for alphabet_letter_smile in LETTERS_SMILES:
            #     result_str += alphabet_letter_smile
            result_str += '\n'

        row_counter = 0
        for row in field:
            if not OUTPUT_DEBUG:
                result_str += NUMBERS_SMILES[row_counter] + ' '
            for cell in row:
                if OUTPUT_DEBUG:
                    result_str += UserGamesManager.__debug_cell_to_str(cell)
                else:
                    result_str += UserGamesManager.__smile_cell_to_str(cell)
            row_counter += 1
            result_str += '\n'
        return result_str[:len(result_str) - 1]

    @staticmethod
    def __debug_cell_to_str(cell):
        return str(cell)

    @staticmethod
    def __smile_cell_to_str(cell):
        if cell == EMPTY:
            return EMPTY_SMILE
        elif cell & DEAD == DEAD:
            return DEAD_SMILE
        elif cell & INJURED == INJURED:
            return INJURED_SMILE
        elif cell & BUFFER_ZONE == BUFFER_ZONE:
            if cell & SHOT == SHOT:
                return SHOT_BUFFER_ZONE_SMILE
            else:
                return UNSHOT_BUFFER_ZONE_SMILE
        elif cell & SHOT == SHOT:
            return SHOT_SMILE
        else:
            return str(cell)

    def __obtain_private_field_from_file(self):
        user_folder_path = self.__create_user_folder()
        private_field_file_path = user_folder_path + 'private'

        try:
            file = open(private_field_file_path)
            file_content = file.readline()
            file.close()
            return ast.literal_eval(file_content)
        except FileNotFoundError:
            return None

    def __obtain_public_field_from_file(self):
        user_folder_path = self.__create_user_folder()
        public_field_file_path = user_folder_path + 'public'

        try:
            file = open(public_field_file_path)
            file_content = file.readline()
            file.close()
            return ast.literal_eval(file_content)
        except FileNotFoundError:
            return None

    def __create_user_folder(self):
        if not os.path.exists(FOLDER_PATH):
            os.makedirs(FOLDER_PATH)

        user_folder_path = FOLDER_PATH + str(self.__user_id) + '/'
        if not os.path.exists(user_folder_path):
            os.makedirs(user_folder_path)

        return user_folder_path

    def __save_game(self):
        user_folder_path = self.__create_user_folder()
        private_field_file_path = user_folder_path + 'private'
        public_field_file_path = user_folder_path + 'public'

        file = open(private_field_file_path, 'w')
        file.write(str(self.__game.get_private_field()))
        file.close

        file = open(public_field_file_path, 'w')
        file.write(str(self.__game.get_user_field()))
        file.close
