from writable import Writable
import vk_api


class VKWriter(Writable):

    def __init__(self, user_id):
        self.__user_id = user_id

    def write(self, text_to_write):
        vk_api.send_message(self.__user_id, text_to_write)
